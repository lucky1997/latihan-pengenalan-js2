var nama = ["Lucky", "Cahyahandika"]
var umur = [23]

console.log("Menggabungkan Array Nama dan Umur")
gab_nama_umur = nama.concat(umur)
console.log(gab_nama_umur)

console.log("Menyelipkan Tahun Lahir pada Array Gabung")
gab_nama_umur.splice(2,0,1997)
console.log(gab_nama_umur)

console.log("Menampilkan Fungsi untuk Menampilkan Elemen Array")
function tampil (array) {
    for (key in array) {
        console.log(array[key])
    }
}

tampil(gab_nama_umur)

